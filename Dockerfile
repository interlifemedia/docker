FROM php:apache

COPY ./php.custom.ini /usr/local/etc/php/conf.d

RUN apt-get update -y
RUN apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libmcrypt-dev \
        libpq-dev
RUN apt-get install -y git unzip
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer
RUN composer self-update

RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql
RUN docker-php-ext-install pgsql pdo_pgsql
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install mbstring
RUN docker-php-ext-install gd

WORKDIR /var/www/html

RUN a2enmod rewrite

EXPOSE 80

CMD ["apache2-foreground"]
